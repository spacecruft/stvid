# stvid
Fork/clone of cbassa's `stvid`.

![stvid processed image](img/2022-08-25T04:35:05.706_48587_catalog.png)

**NOTE: Unless you have a particular reason to use this, you should use
the main upstream repository.**

https://github.com/cbassa/stvid


# Installation
Debian. Install package dependencies:

```
sudo apt install python3-virtualenv python3-pip python3-dev \
    source-extractor astrometry.net \
    libeigen3-dev giza-dev libx11-dev
```

Perhaps need some of the `astrometry.net` data files:

```
sudo apt install \
    astrometry-data-tycho2 astrometry-data-tycho2-07 \
    astrometry-data-tycho2-08 astrometry-data-tycho2-09 \
    astrometry-data-tycho2-10-19
```


```
git clone https://spacecruft.org/spacecruft/stvid
# Or use upstream:
git clone https://github.com/cbassa/stvid
```

Optionally, set up python virtual environment:

```
cd stvid
virtualenv -p python3 env
source env/bin/activate
pip install --upgrade pip
```

Install dependencies. Numpy needs to be installed first, or the
install fails.


```
pip install --upgrade numpy
pip install --upgrade -r requirements.txt
```

If the system `python3-numpy` conflicts, you could try installing
numpy in the virtualenv thusly:

```
pip install --upgrade --ignore-installed numpy
```


Debian in bug #941466 changed the name of the `sextractor` dependency to
`source-extractor`, so a symlink is needed for now:

```
sudo ln -s /usr/bin/source-extractor /usr/local/bin/sextractor
```

Install hough3d-code dependency:

```
cd ../
git clone https://gitlab.com/pierros/hough3d-code
cd hough3d-code
make all
sudo cp -p hough3dlines /usr/local/bin/hough3dlines
```

Install `satpredict`:

```
cd ../
git clone https://spacecruft.org/spacecruft/satpredict
# Or
#git clone https://github.com/cbassa/satpredict
cd satpredict/
make
sudo make install
```

# Configure
Thusly...

```
cd stvid/
cp -p configuration.ini-dist configuration.ini
# Edit
# Note for fields st-username and st-password, *don't* use quotes.
# Set:
vi configuration.ini
```

Edit various `configuration.ini` fields:
```
# For example:
observer_cospar = 9999        # COSPAR number of observer's site
observer_name = Jeff Moe      # Name of observer
observer_lat = 40.568912      # Latitude of location in decimal degrees
observer_lon = -105.225852    # Longitude of location in decimal degrees
observer_height = 1860         # Elevation of location in meters

observations_path = ./data/obs # Path for raw acquisitions
tle_path = ./data/tle          # Path where TLEs are stored (and updated)
results_path = ./data/results   # Path for results of processed obs

st-username = foo
st-password = bar

# Path to source-extractor
sex_config = /usr/share/source-extractor/default.sex
```

Store downloads here:

```
mkdir -p ./data/obs ./data/tle ./data/results
```

The `ST_DATADIR` variable is still used, from `sattools`.
Maybe set all these in `~/.bashrc` (suit to fit directory setup):

```
export ST_DATADIR=$HOME/sattools
export ST_TLEDIR=$HOME/TLE
export ST_OBSDIR=$HOME/satobs
export ST_LOGIN="identity=foo@no:pass"
```


Set `astrometry.net` to run in parallel, assuming you have enough RAM:
(This doesn't appear to work? Breaks?).

```
sudo vim /etc/astrometry.cfg
# Uncomment:
inparallel
```


# Hardware
Setup with various hardware.

Specs:

* Sony IMX174 or similar.
* 50mm lens.
* f1.2 to f1.6 (faster the better).
* 10Hz image sampling.
* USB-3 (or ethernet?).

For permanent mounts, considerations:

* IP-66 or better.
* PoE?
* 12V.
* Raspberry Pi, Intel NUC, Traverse Ten64.
* Hardware shutter during sunlight (?).

Proprietary examples with IMX174:

* QHYCCD QHY174GPS.
* ZWO ASI174MM.


## The Imaging Source
The Imaging Source camera confirmed works well, such as this:

* https://www.theimagingsource.com/products/industrial-cameras/usb-3.0-monochrome/dmk33ux174/

See `README-TIS.md` for more info.


## Libre
Trying to find libre setup that can be used.


Solutions with `gphoto` or other non-video camera setups
should use `stphot` not `stvid`.


## Proprietary
The ASI ZWO and QHY cameras use a proprietary SDK.
There are no libre implementations found.


## Web Cam
Perhaps like this using a web cam and OpenCV

Set up udev:
```
echo 'SUBSYSTEM=="video1", GROUP="video", MODE="0660"' |
  sudo tee /etc/udev/rules.d/10-webcam.rules
```

Testing with Logitech BRIO usb web cam. No udev configuration needed.
Camera runs with `stvid` but isn't sensitive enough to be used.


# Usage
Enter `virtualenv`, if used:

```
cd stvid/
source env/bin/activate
```

Get the latest TLEs:

```
./update_tle.py
```

Configure a position (example). XXX Best way to generate ?

```
echo \
 '2022-08-07T23:44:14 R: 00:00:00.00 D:  00:00:00.0 H: -152.691 A: 180.000 E: 90.000 q: -28.18' |
 > data/obs/control/position.txt

# Maybe
echo \
 '2022-08-07T23:44:14 R: 00:00:00.00 D:  00:00:00.0 H: -152.691 A: 180.000 E: 90.000 q: -28.18' |
 > $ST_DATADIR/data/position.txt

```

Acquire image, first download archives:

```
./acquire.py
```

![stvid acquired image](img/stvid-acquire.png)
*stvid acquired image, viewed with astroimagej.*


## Preview
This scriptlet isn't in upstream. It does a preview of the camera using the
standard configuration file. So you can get a preview similar to what it actually
looks like for focus, perhaps. Does not require X running, but it does
need `xinit` installed.
Also need to set `X11Forwarding yes` in `/etc/ssh/sshd_config`.
Then login to your host with `-XY` ala:

```
ssh -XY odroidn2
cd stvid
source env/bin/activate
./preview.py
```


# Fixed
When running `acquire.py` on `amd64` architecture it gives this error:

```
OSError: libbcm_host.so: cannot open shared object file: No such file or directory
```

The `libbcm_host.so` is a (non-libre?) firmware file that is part of Raspberry Pi.

https://github.com/raspberrypi/firmware/blob/master/opt/vc/lib/libbcm_host.so

That error comes from installing `picamerax` on `amd64`, which was added to
`requirements.txt`, but should really just be when on Raspberry Pi.

```
env/lib/python3.10/site-packages/picamerax/bcm_host.py:_lib = ct.CDLL('libbcm_host.so')
```

For now, just import the library that calls for that binary if the
Pi camera is used. No need to call it (and crash) if CV2 is used on non-Pi.


# Upstream
See `README-upstream.md`.

`stvid`:

* https://github.com/cbassa/stvid

&copy; 2018-2022 Cees Bassa


# License
Same as upstream license, GPLv3.

&copy; 2022, Jeff Moe
