# The Imaging Source
Equipment from The Imaging Source is currently being evaluated.

* https://www.theimagingsource.com


# Sensors

DMK 33UX174

* https://www.theimagingsource.com/products/industrial-cameras/usb-3.0-monochrome/dmk33ux174/


# Lenses
V5014-MP

F1.4 version of lens listed here (e.g. V5024-MPZ which is F2.4)

* https://www.theimagingsource.com/products/optics/lenses/high-end/


# Documentation
User documentation for The Imaging Source Linux libraries.
`tiscamera`:
* https://www.theimagingsource.com/documentation/tiscamera/


# Source Code
Code is available under libre licenses.
The Imaging Source tends to use the Apache license for their files.

The Linux SDK for The Imaging Source cameras
* https://github.com/TheImagingSource/tiscamera

Programming samples in Python and C++ for the tiscamera GStreamer modules.
* https://github.com/TheImagingSource/Linux-tiscamera-Programming-Samples

Source code to the flasher is libre, but the code it flashes to the
cameras is proprietary, afaict.

Linux USB Firmware Update Tool
* https://github.com/TheImagingSource/tcam-firmware-update


# Build
Build tools thusly.

**XXX NOTE XXX**

The first process may involve some non-libre files.
Further investigation is needed. After compile, see:

`./build/src/v4l2/CMakeFiles/tcam-backend-v4l2.dir`

The second process listed below may avoid the blobs.

```
git clone https://github.com/TheImagingSource/tiscamera
cd tiscamera
# Review command, runs with sudo:
./scripts/dependency-manager install
mkdir build
cd build/
cmake -DTCAM_BUILD_ARAVIS=ON -DTCAM_BUILD_TOOLS=ON -DTCAM_BUILD_V4L2=ON \
  -DTCAM_BUILD_LIBUSB=ON -DTCAM_BUILD_DOCUMENTATION=ON ..
# The following will potentially download some non-libre `.o` files.
make
# The following will potentially install some non-libre `.o` files.
sudo make install
```

Perhaps to avoid the non-free bits and keep the final installation
libre, it could be done something like this:

```
git clone https://github.com/TheImagingSource/tiscamera
cd tiscamera
# Review command, runs with sudo:
./scripts/dependency-manager install
mkdir build
cd build/
cmake -DTCAM_BUILD_ARAVIS=ON -DTCAM_BUILD_TOOLS=ON -DTCAM_BUILD_V4L2=ON \
  -DTCAM_BUILD_LIBUSB=ON -DTCAM_BUILD_DOCUMENTATION=ON ..
# The following will potentially download some non-libre `.o` files.
make
sudo mkdir -p /usr/share/theimagingsource/tiscamera/uvc-extension/ 
sudo cp ./data/uvc-extensions/usb33.json /usr/share/theimagingsource/tiscamera/uvc-extension/
sudo cp ./build/data/udev/80-theimagingsource-cameras.rules /etc/udev/rules.d/
sudo cp ./build/bin/tcam-gigetool /usr/bin/
sudo cp ./build/bin/tcam-uvc-extension-loader /usr/bin/
```


# stvid
Works with `stvid` and a configuration snippet like this:

```
[Camera]
camera_type = CV2

[CV2]
device_id = 0
nx = 1920
ny = 1080
nframes = 250
```

# Hardware Info
This is the output from `v4l2-ctl --all`.

```
Driver Info:
	Driver name      : uvcvideo
	Card type        : DMK 33UX174
	Bus info         : usb-0000:00:14.0-5
	Driver version   : 5.18.16
	Capabilities     : 0x84a00001
		Video Capture
		Metadata Capture
		Streaming
		Extended Pix Format
		Device Capabilities
	Device Caps      : 0x04200001
		Video Capture
		Streaming
		Extended Pix Format
Media Driver Info:
	Driver name      : uvcvideo
	Model            : DMK 33UX174
	Serial           : 30220539
	Bus info         : usb-0000:00:14.0-5
	Media version    : 5.18.16
	Hardware revision: 0x00000000 (0)
	Driver version   : 5.18.16
Interface Info:
	ID               : 0x03000002
	Type             : V4L Video
Entity Info:
	ID               : 0x00000001 (1)
	Name             : DMK 33UX174
	Function         : V4L2 I/O
	Flags            : default
	Pad 0x01000007   : 0: Sink
	  Link 0x02000010: from remote pad 0x100000a of entity 'Extension 3' (Video Pixel Formatter): Data, Enabled, Immutable
Priority: 2
Video input : 0 (Camera 1: ok)
Format Video Capture:
	Width/Height      : 1920/1080
	Pixel Format      : 'Y16 ' (16-bit Greyscale)
	Field             : None
	Bytes per Line    : 3840
	Size Image        : 4147200
	Colorspace        : sRGB
	Transfer Function : Default (maps to sRGB)
	YCbCr/HSV Encoding: Default (maps to ITU-R 601)
	Quantization      : Default (maps to Full Range)
	Flags             : 
Crop Capability Video Capture:
	Bounds      : Left 0, Top 0, Width 1920, Height 1080
	Default     : Left 0, Top 0, Width 1920, Height 1080
	Pixel Aspect: 1/1
Selection Video Capture: crop_default, Left 0, Top 0, Width 1920, Height 1080, Flags: 
Selection Video Capture: crop_bounds, Left 0, Top 0, Width 1920, Height 1080, Flags: 
Streaming Parameters Video Capture:
	Capabilities     : timeperframe
	Frames per second: 30.000 (30/1)
	Read buffers     : 0

User Controls

                     brightness 0x00980900 (int)    : min=0 max=511 step=1 default=240 value=151
                          gamma 0x00980910 (int)    : min=1 max=500 step=1 default=100 value=126
                           gain 0x00980913 (int)    : min=0 max=480 step=1 default=0 value=23

Camera Controls

                  auto_exposure 0x009a0901 (menu)   : min=0 max=3 default=3 value=1 (Manual Mode)
				1: Manual Mode
				3: Aperture Priority Mode
         exposure_time_absolute 0x009a0902 (int)    : min=1 max=300000 step=1 default=3 value=1
               exposure_time_us 0x0199e201 (int)    : min=20 max=30000000 step=1 default=333 value=20
                   auto_shutter 0x0199e202 (bool)   : default=1 value=0
        auto_exposure_reference 0x0199e203 (int)    : min=0 max=255 step=1 default=128 value=117
                      gain_auto 0x0199e205 (bool)   : default=1 value=0
                   trigger_mode 0x0199e208 (bool)   : default=0 value=0
               software_trigger 0x0199e209 (button) : value=0 flags=write-only
                  strobe_enable 0x0199e211 (bool)   : default=0 value=0
                strobe_polarity 0x0199e212 (bool)   : default=0 value=0
               strobe_operation 0x0199e213 (menu)   : min=0 max=1 default=1 value=1 (Exposure)
				0: Fixed Duration
				1: Exposure
                strobe_duration 0x0199e214 (int)    : min=0 max=32767 step=1 default=100 value=100
                   strobe_delay 0x0199e215 (int)    : min=0 max=32767 step=1 default=0 value=0
                          gpout 0x0199e216 (bool)   : default=0 value=0
                           gpin 0x0199e217 (bool)   : default=0 value=0
                   roi_offset_x 0x0199e218 (int)    : min=0 max=1664 step=2 default=0 value=0
                   roi_offset_y 0x0199e219 (int)    : min=0 max=1196 step=2 default=0 value=60
                roi_auto_center 0x0199e220 (bool)   : default=1 value=1
               trigger_polarity 0x0199e234 (menu)   : min=0 max=1 default=0 value=1 (Falling Edge)
				0: Rising Edge
				1: Falling Edge
          trigger_exposure_mode 0x0199e236 (menu)   : min=0 max=1 default=0 value=0 (Frame Start)
				0: Frame Start
				1: Exposure Active
            trigger_burst_count 0x0199e237 (int)    : min=1 max=1000 step=1 default=1 value=1
       trigger_debounce_time_us 0x0199e238 (int)    : min=0 max=1000000 step=1 default=0 value=0
           trigger_mask_time_us 0x0199e239 (int)    : min=0 max=1000000 step=1 default=0 value=0
 trigger_noise_suppression_time 0x0199e240 (int)    : min=0 max=1000000 step=1 default=0 value=0
     auto_functions_roi_control 0x0199e241 (bool)   : default=1 value=0
        auto_functions_roi_left 0x0199e242 (int)    : min=0 max=1904 step=1 default=0 value=480
         auto_functions_roi_top 0x0199e243 (int)    : min=0 max=1184 step=1 default=0 value=300
       auto_functions_roi_width 0x0199e244 (int)    : min=16 max=1920 step=1 default=0 value=960
      auto_functions_roi_height 0x0199e245 (int)    : min=16 max=1200 step=1 default=0 value=600
                flip_horizontal 0x0199e251 (bool)   : default=0 value=0
                  flip_vertical 0x0199e252 (bool)   : default=0 value=0
 exposure_auto_upper_limit_auto 0x0199e254 (bool)   : default=1 value=0
   exposure_auto_lower_limit_us 0x0199e255 (int)    : min=20 max=1000000 step=1 default=333 value=30
   exposure_auto_upper_limit_us 0x0199e256 (int)    : min=20 max=1000000 step=1 default=333 value=33333
         override_scanning_mode 0x0199e257 (int)    : min=1 max=16 step=1 default=1 value=1
      auto_functions_roi_preset 0x0199e258 (menu)   : min=0 max=5 default=2 value=2 (Center 50%)
				0: Full Sensor
				1: Custom Rectangle
				2: Center 50%
				3: Center 25%
				4: Bottom Half
				5: Top Half
   trigger_global_reset_release 0x0199e261 (bool)   : default=0 value=0
         scanning_mode_selector 0x0199e263 (int)    : min=1 max=16 step=1 default=1 value=1
       scanning_mode_identifier 0x0199e264 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
 scanning_mode_scale_horizontal 0x0199e265 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
   scanning_mode_scale_vertical 0x0199e266 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
        scanning_mode_binning_h 0x0199e267 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
        scanning_mode_binning_v 0x0199e268 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
       scanning_mode_skipping_h 0x0199e269 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
       scanning_mode_skipping_v 0x0199e270 (int)    : min=1 max=1 step=1 default=1 value=1 flags=read-only
            scanning_mode_flags 0x0199e271 (int)    : min=0 max=0 step=0 default=0 value=0 flags=read-only
            trigger_delay_100ns 0x0199e272 (int)    : min=0 max=1000000 step=1 default=0 value=31
```
